package com.ut.algorithmics.shortestpathalgorithmsdemo.rest;

import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.ClientBody;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.NodeInfo;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Point;
import com.ut.algorithmics.shortestpathalgorithmsdemo.service.AlgorithmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Algorithms controller.
 */

@RestController
@RequestMapping(value="/algorithms")
@CrossOrigin
public class AlgorithmsController {

    @Autowired
    AlgorithmsService algorithmsService;

    @RequestMapping(value="/bfs", method = RequestMethod.POST)
    @ResponseBody
    public List<NodeInfo> startBfs(@RequestBody ClientBody clientBody){
        return algorithmsService.startBfs(clientBody.startPoint, clientBody.pointList, clientBody.edgeList);
    }

    @RequestMapping(value="/dfs", method = RequestMethod.POST)
    @ResponseBody
    public List<NodeInfo> startDfs(@RequestBody ClientBody clientBody){
        return algorithmsService.startDfs(clientBody.startPoint, clientBody.pointList, clientBody.edgeList);
    }

    @RequestMapping(value="/dijkstra", method = RequestMethod.POST)
    @ResponseBody
    public List<NodeInfo> startDijkstra(@RequestBody ClientBody clientBody){
        return algorithmsService.startDijkstra(clientBody.startPoint, clientBody.endPoint, clientBody.pointList,
                clientBody.edgeList);
    }

    @RequestMapping(value="/astar", method = RequestMethod.POST)
    @ResponseBody
    public List<NodeInfo> startAStar(@RequestBody ClientBody clientBody){
        return algorithmsService.startAStar(clientBody.startPoint, clientBody.endPoint, clientBody.obstacleList,
                clientBody.width, clientBody.height);
    }


}
