package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

public class NodeInfo {
    private String label;
    private int x;
    private int y;

    public NodeInfo(){
    }

    public NodeInfo(String label, int x, int y) {
        this.label = label;
        this.x = x;
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object object){
        NodeInfo nodeInfo = (NodeInfo) object;
        return this.getLabel().equals(nodeInfo.getLabel());
    }

    public int compare(NodeInfo nodeInfo){
        return this.getLabel().compareTo(nodeInfo.getLabel());
    }

    public double getDistance(NodeInfo nodeInfo){
        int y = Math.abs(nodeInfo.getY() - this.getY());
        int x = Math.abs(nodeInfo.getY() - this.getY());
        return Math.sqrt(y*y + x*x);
    }
}
