package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

public class NodeNeighbor<E> {
    private Node node;
    private double weight;

    public NodeNeighbor(Node node, double weight){
        this.node = node;
        this.weight = weight;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
