package com.ut.algorithmics.shortestpathalgorithmsdemo.service;

import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Edge;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.NodeInfo;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Obstacle;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Point;

import java.util.List;

/**
 * Interface for algorithms(bfs, dfs, dijkstra, astar).
 */
public interface AlgorithmsService {

    /**
     * Performs bfs from a given start point.
     *
     * @param startPoint
     * @param pointList
     * @param edgeList
     * @return the list of nodes
     */
    List<NodeInfo> startBfs(NodeInfo startPoint, List<NodeInfo> pointList, List<Edge> edgeList);

    /**
     * Performs dfs from a given start point.
     *
     * @param startPoint
     * @param pointList
     * @param edgeList
     * @return the list of nodes
     */
    List<NodeInfo> startDfs(NodeInfo startPoint, List<NodeInfo> pointList, List<Edge> edgeList);

    /**
     * Perfroms dijkstra between two points.
     *
     * @param startPoint
     * @param endPoint
     * @param pointList
     * @param edgeList
     * @return the shortest path between two points
     */
    List<NodeInfo> startDijkstra(NodeInfo startPoint, NodeInfo endPoint, List<NodeInfo> pointList, List<Edge> edgeList);

    /**
     * Perform A* from a given start point to an end point with a list of obstacles.
     * @param startPoint
     * @param endPoint
     * @param obstacleList
     * @return the shortest path between the start point and the end point
     */
    List<NodeInfo> startAStar(NodeInfo startPoint, NodeInfo endPoint, List<Obstacle> obstacleList, int width, int height);
}
