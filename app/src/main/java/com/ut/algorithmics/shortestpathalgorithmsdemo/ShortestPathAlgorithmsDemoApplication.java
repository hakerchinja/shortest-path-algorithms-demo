package com.ut.algorithmics.shortestpathalgorithmsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShortestPathAlgorithmsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShortestPathAlgorithmsDemoApplication.class, args);
	}

}
