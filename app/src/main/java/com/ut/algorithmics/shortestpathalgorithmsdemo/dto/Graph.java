package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

import java.util.*;

public class Graph {
    private ArrayList<Node> nodes;

    public Graph(List<NodeInfo> nodeInfos, List<Edge> edges, boolean isDirected){
        nodes = new ArrayList<>();

        int i = 0;
        for(NodeInfo nodeInfo : nodeInfos){
            Node node = new Node(i, nodeInfo);
            nodes.add(node);
            i++;
        }

        for(Edge edge : edges){
            Node from = getNodeByLabel(edge.getNodeFrom());
            Node to = getNodeByLabel(edge.getNodeTo());
            from.addNeighbor(to, edge.getWeight());

            if(!isDirected){
                to.addNeighbor(from, edge.getWeight());
            }
        }
    }

    public Node getNodeByLabel(String label){
        for(Node node : nodes){
            if(node.getInfo().getLabel().equals(label)){
                return node;
            }
        }
        return null;
    }

    public Node getNodeByIndex(int index){
        for(Node node : nodes){
            if(node.getIndex() == index){
                return node;
            }
        }
        return null;
    }

    public int getNumberOfNodes(){
        return nodes.size();
    }


    /**
     * DFS search
     * */
    public List<NodeInfo> dfs(String label){
        List<NodeInfo> nodeInfos = new ArrayList<>();
        int numNodes = getNumberOfNodes();
        boolean[] visited = new boolean[numNodes];
        for(int i=0;i<numNodes;i++){
            visited[i] = false;
        }
        Node node = getNodeByLabel(label);
        int currIndex = node.getIndex();
        dfsRecursive(currIndex, visited, nodeInfos);
        return nodeInfos;
    }

    private void dfsRecursive(int index, boolean[] visited, List<NodeInfo> nodeInfos){
        visited[index] = true;
        nodeInfos.add(getNodeByIndex(index).getInfo());
        Node node = getNodeByIndex(index);

        LinkedList<NodeNeighbor> neighbors = node.getSortedNeighbors();
        for(int i=0;i<neighbors.size();i++){
            Node currNode = neighbors.get(i).getNode();
            if(!visited[currNode.getIndex()]){
                dfsRecursive(currNode.getIndex(), visited, nodeInfos);
            }
        }
    }

    /**
     * BFS search
     * */
    public List<NodeInfo> bfs(int index){
        List<NodeInfo> nodeInfos = new ArrayList<>();

        Map<Integer, Boolean> visited = new HashMap<>();

        for(int i=0;i<nodes.size();i++){
            visited.put(nodes.get(i).getIndex(), false);
        }

        visited.replace(index, true);

        int depth = 1;

        LinkedQueue<Integer> q = new LinkedQueue<Integer>();
        q.enqueue(index);
        nodeInfos.add(getNodeByIndex(index).getInfo());

        Node temp;

        while(!q.isEmpty()){
            temp = getNodeByIndex(q.dequeue());

            Node other;

            for(int i=0;i<temp.getNumberOfNeighbors();i++){
                other = temp.getNeighborByIndex(i);
                if(!visited.get(other.getIndex())){
                    visited.replace(other.getIndex(), true);
                    q.enqueue(other.getIndex());
                    depth++;
                    nodeInfos.add(other.getInfo());
                }
            }
        }
        return nodeInfos;
    }

    /**
     * Dijkstra
     * */
    public double[] dijkstra(String label){
        int numberOfNodes = getNumberOfNodes();
        double distance[] = new double[numberOfNodes];
        boolean isFinal[] = new boolean[numberOfNodes];

        for (int i = 0; i < numberOfNodes; i++) {
            distance[i] = -1;
            isFinal[i] = false;
        }

        Node currNode = getNodeByLabel(label);
        int currIndex = currNode.getIndex();

        isFinal[currIndex] = true;
        distance[currIndex] = 0;

        for (int i = 1; i < numberOfNodes; i++) {
            Iterator<NodeNeighbor> it = currNode.getNeighbors().iterator();
            while (it.hasNext()) {
                NodeNeighbor curr = it.next();
                if (!isFinal[curr.getNode().getIndex()]) {
                    int neighborIndex = curr.getNode().getIndex();
                    if (distance[neighborIndex] <= 0) {
                        distance[neighborIndex] = distance[currIndex] + curr.getWeight();
                    } else if (distance[currIndex] + curr.getWeight() < distance[neighborIndex]) {
                        distance[neighborIndex] = distance[currIndex] + curr.getWeight();
                    }
                }
            }
            boolean mInit = false;
            int k = -1;
            double mInitialized = -1;
            for (int j = 0; j < numberOfNodes; j++) {
                if (!isFinal[j] && distance[j] != -1) {
                    if (!mInit) {
                        mInitialized = distance[k = j];
                        mInit = true;
                    } else if (mInitialized > distance[j] && distance[j] > 0){
                        mInitialized = distance[k = j];
                    }
                }
            }
            isFinal[currIndex = k] = true;
            currNode = getNodeByIndex(currIndex);
        }
        return distance;
    }

    public Comparator<Integer> PQComparator = new Comparator<Integer>() {

        public int compare(Integer a, Integer b) {
            return Integer.compare(a, b);
        }
    };

    public List<NodeInfo> dijkstra(int vertexA, int vertexB) {
        Map<Integer, Integer> previousNode = new HashMap<>();
        Map<Integer, Double> weight = new HashMap<>();
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>(getNumberOfNodes(), PQComparator);

        for (Node vertex : nodes)
            weight.put(vertex.getIndex(), Double.POSITIVE_INFINITY);

        previousNode.put(vertexA, -1); // negative means no previous vertex
        weight.put(vertexA, 0.0); // weight to has to be 0
        pq.add(vertexA); // enqueue first vertex

        while (pq.size() > 0) {
            int currentNode = pq.poll();
            Node currNode = getNodeByIndex(currentNode);
            LinkedList<NodeNeighbor> neighbors = currNode.getNeighbors();

            if (neighbors == null) continue;

            for (NodeNeighbor neighbor : neighbors) {
                int nextVertex = neighbor.getNode().getIndex();

                double newDistance = weight.get(currentNode) + neighbor.getWeight();
                if (weight.get(nextVertex) == Double.POSITIVE_INFINITY) {
                    previousNode.put(nextVertex, currentNode);
                    weight.put(nextVertex, newDistance);
                    pq.add(nextVertex);
                } else {
                    if (weight.get(nextVertex) > newDistance) {
                        previousNode.put(nextVertex, currentNode);
                        weight.put(nextVertex, newDistance);
                    }
                }
            }
        }
          /* Path from A to B will be stored here */
        ArrayList<Integer> nodePath = new ArrayList<Integer>();

        Stack<Integer> nodePathTemp = new Stack<Integer>();
        nodePathTemp.push(vertexB);

        int v = vertexB;
        while (previousNode.containsKey(v) && previousNode.get(v) >= 0 && v > 0) {
            v = previousNode.get(v);
            nodePathTemp.push(v);
        }

        // Put node in ArrayList in reversed order
        while (nodePathTemp.size() > 0)
            nodePath.add(nodePathTemp.pop());


        List<NodeInfo> nodeInfos = new ArrayList<>();
        for(Integer index : nodePath){
            nodeInfos.add(getNodeByIndex(index).getInfo());
        }

        return nodeInfos;
    }
}


