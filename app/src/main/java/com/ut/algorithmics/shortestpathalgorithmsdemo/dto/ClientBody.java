package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class ClientBody {

    @JsonProperty("startPoint")
    public NodeInfo startPoint;

    @JsonProperty("endPoint")
    public NodeInfo endPoint;

    @JsonProperty("pointList")
    public List<NodeInfo> pointList;

    @JsonProperty("edgeList")
    public List<Edge> edgeList;

    @JsonProperty("obstacles")
    public List<Obstacle> obstacleList;

    @JsonProperty("width")
    public int width;

    @JsonProperty("height")
    public int height;
}
