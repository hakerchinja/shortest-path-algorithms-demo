package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

import com.ut.algorithmics.shortestpathalgorithmsdemo.helper.Helper;

public class AStarNode {
    private int x, y;

    private double cost;

    private AStarNode prevNode;

    private double calculatedCost;
    private double estimatedCost;

    public AStarNode(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public AStarNode getPrevNode() {
        return prevNode;
    }

    public void setPrevNode(AStarNode prevNode) {
        this.prevNode = prevNode;
    }

    public double getCalculatedCost() {
        return calculatedCost;
    }

    public void setCalculatedCost(AStarNode prevNode){
        this.calculatedCost = prevNode.getCalculatedCost() + cost;
    }

    public double getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(AStarNode endNode) {
        this.estimatedCost = Helper.distance(this.getX(), this.getY(), endNode.getX(), endNode.getY());
    }

    public double calculateCost(AStarNode prevNode){
        return prevNode.getCalculatedCost() + cost;
    }

    @Override
    public boolean equals(Object object){
        AStarNode node = null;
        try{
            node = (AStarNode) object;
            return node.getX() == this.getX() && node.getY() == this.getY();
        } catch(Exception e){

        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }
}
