package com.ut.algorithmics.shortestpathalgorithmsdemo.algorithms;

import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.AStarNode;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Obstacle;
import com.ut.algorithmics.shortestpathalgorithmsdemo.helper.Helper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AStarSearch {
    private AStarNode[][] nodes;
    private int width, height;

    private List<AStarNode> allVisitedNodes;

    private List<Obstacle> obstacles;

    public AStarSearch(int width, int height, List<Obstacle> obstacles) {
        this.width = width;
        this.height = height;
        this.obstacles = obstacles;

        nodes = new AStarNode[width][height];
        allVisitedNodes = new ArrayList<>();

        initNodes();
        initObstacles();
    }

    public List<AStarNode> getAllVisitedNodes(){
        return allVisitedNodes;
    }

    private void initNodes(){
        for(int i=0;i<width;i++){
            for(int j=0;j<height;j++){
                nodes[i][j] = new AStarNode(i, j);
            }
        }
    }

    private void initObstacles(){
        for(int i=0;i<width;i++){
            for(int j=0;j<height;j++){
                for(Obstacle obstacle : obstacles){
                    if(obstacle.isInObstacle(i, j)){
                        nodes[i][j].setCost(obstacle.getCost());
                    }
                }

            }
        }
    }

    public LinkedList<AStarNode> findShortestPath(int fromX, int fromY, int toX, int toY){
        LinkedList<AStarNode> openList = new LinkedList<>();
        LinkedList<AStarNode> closedList = new LinkedList<>();
        openList.add(nodes[fromX][fromY]);

        boolean done = false;

        AStarNode currentNode;

        allVisitedNodes = new ArrayList<>();
        while(!done){
            currentNode = getGetLowestCostNodeInOpenList(openList);
            allVisitedNodes.add(currentNode);
            closedList.add(currentNode);
            openList.remove(currentNode);

            if(currentNode.getX() == toX && currentNode.getY() == toY){
                return calculatePath(nodes[fromX][fromY], currentNode);
            }

            List<AStarNode> adjacentNodes = getAdjacentNodes(currentNode, closedList);
            for(AStarNode currentNeighbor : adjacentNodes){
                allVisitedNodes.add(currentNeighbor);
                if(!openList.contains(currentNeighbor)){
                    currentNeighbor.setPrevNode(currentNode);
                    currentNeighbor.setCalculatedCost(currentNode);
                    currentNeighbor.setEstimatedCost(nodes[toX][toY]);
                    openList.add(currentNeighbor);
                } else {
                    if(currentNeighbor.getCalculatedCost() > currentNeighbor.calculateCost(currentNode)){
                        currentNeighbor.setPrevNode(currentNode);
                        currentNeighbor.setCalculatedCost(currentNode);
                    }
                }
            }
            if (openList.isEmpty()) { // no path exists
                return new LinkedList<AStarNode>(); // return empty list
            }
        }
        return null; //unreachable
    }

    private List<AStarNode> getAdjacentNodes(AStarNode node, LinkedList<AStarNode> closedList){
        int x = node.getX();
        int y = node.getY();
        List<AStarNode> adj = new LinkedList<>();

        AStarNode curr;

        if(x > 1){
            curr = nodes[x-1][y];
            if(!closedList.contains(curr)){
                adj.add(curr);
            }
        }

        if(x < width - 1){
            curr = nodes[x+1][y];
            adj.add(curr);
            if(!closedList.contains(curr)){
                adj.add(curr);
            }
        }

        if(y > 1){
            curr = nodes[x][y-1];
            if(!closedList.contains(curr)){
                adj.add(curr);
            }
        }

        if(y < height - 1){
            curr = nodes[x][y+1];
            if(!closedList.contains(curr)){
                adj.add(curr);
            }
        }

        return adj;
    }

    private LinkedList<AStarNode> calculatePath(AStarNode from, AStarNode to){
        LinkedList<AStarNode> path = new LinkedList<>();
        AStarNode currentNode = to;
        boolean done = false;
        while(!done){
            path.addFirst(currentNode);
            currentNode = currentNode.getPrevNode();

            if(currentNode.equals(from)){
                done = true;
            }
        }
        return path;
    }

    private AStarNode getGetLowestCostNodeInOpenList(LinkedList<AStarNode> openList){
        AStarNode cheapest = openList.get(0);
        for(int i=0;i<openList.size();i++){
            AStarNode currNode = openList.get(i);
            if((currNode.getEstimatedCost() + currNode.getCalculatedCost())
                    < (cheapest.getEstimatedCost() + cheapest.getCalculatedCost())){
                cheapest = currNode;
            }
        }
        return cheapest;
    }

    public double getCostOfNode(int x, int y){
        return this.nodes[x][y].getCalculatedCost();
    }
}
