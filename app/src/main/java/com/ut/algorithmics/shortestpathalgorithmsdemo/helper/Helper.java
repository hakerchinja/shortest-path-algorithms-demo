package com.ut.algorithmics.shortestpathalgorithmsdemo.helper;

public class Helper {

    public static double distance(int x1, int y1, int x2, int y2){
        return Math.sqrt((x1-x2)*(x1-x2) +
                (y1-y2)*(y1-y2));
    }

    public static boolean isInCircle(int x, int y, int cx, int cy, int radius){
        return (x - cx)*(x - cx) + (y - cy)*(y - cy) < radius*radius;
    }

    public static boolean isInRect(int x, int y, int cx, int cy, int width, int height){
        return (cx < x) && (cx + width > x) && (cy < y) && (cy + height > y);
    }
}
