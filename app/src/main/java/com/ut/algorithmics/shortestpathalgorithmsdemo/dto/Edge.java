package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

public class Edge {
    private String nodeFrom;
    private String nodeTo;
    private double weight;

    public Edge(){

    }

    public Edge(String nodeFrom, String nodeTo, double weight){
        this.nodeFrom = nodeFrom;
        this.nodeTo = nodeTo;
        this.weight = weight;
    }

    public String getNodeFrom() {
        return nodeFrom;
    }

    public void setNodeFrom(String nodeFrom) {
        this.nodeFrom = nodeFrom;
    }

    public String getNodeTo() {
        return nodeTo;
    }

    public void setNodeTo(String nodeTo) {
        this.nodeTo = nodeTo;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
