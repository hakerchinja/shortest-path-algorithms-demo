package com.ut.algorithmics.shortestpathalgorithmsdemo.rest;

import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.ClientBody;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Edge;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.NodeInfo;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.Obstacle;
import com.ut.algorithmics.shortestpathalgorithmsdemo.service.AlgorithmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/mock")
@CrossOrigin
public class MockController {

    @Autowired
    AlgorithmsService algorithmsService;

    @RequestMapping(value="/bfs", method = RequestMethod.GET)
    public List<NodeInfo> startBfs(){
        List<NodeInfo> nodeInfos = getMockNodeInfos();
        List<Edge> edges = getMockEdges();
        return algorithmsService.startBfs(nodeInfos.get(0), nodeInfos, edges);
    }

    @RequestMapping(value="/dfs", method = RequestMethod.GET)
    public List<NodeInfo> startDfs(){
        List<NodeInfo> nodeInfos = getMockNodeInfos();
        List<Edge> edges = getMockEdges();
        return algorithmsService.startDfs(nodeInfos.get(0), nodeInfos, edges);
    }

    @RequestMapping(value="/dijkstra", method = RequestMethod.GET)
    public List<NodeInfo> startDijkstra(){
        List<NodeInfo> nodeInfos = getMockNodeInfos();
        List<Edge> edges = getMockEdges();
        return algorithmsService.startDijkstra(nodeInfos.get(0), nodeInfos.get(3), nodeInfos,
                edges);
    }

    @RequestMapping(value="/astar", method = RequestMethod.GET)
    public List<NodeInfo> startAStar(){
        int width = 50;
        int height = 50;

        NodeInfo from = new NodeInfo("a", 10, 25);
        NodeInfo to = new NodeInfo("b", 20, 25);

        List<Obstacle> obstacles = new ArrayList<>();
        obstacles.add(new Obstacle(25, 5, 5, 0, 400, true));

        return algorithmsService.startAStar(from, to, obstacles, width, height);
    }


    private List<NodeInfo> getMockNodeInfos(){
        List<NodeInfo> nodeInfos = new ArrayList<>();
        nodeInfos.add(new NodeInfo("A", 10, 10));
        nodeInfos.add(new NodeInfo("B", 20, 20));
        nodeInfos.add(new NodeInfo("C", 10, 30));
        nodeInfos.add(new NodeInfo("D", 40, 20));
        return nodeInfos;
    }

    private List<Edge> getMockEdges(){
        List<Edge> edges = new ArrayList<>();
        edges.add(new Edge("A", "B", 2));
        edges.add(new Edge("B", "C", 3));
        edges.add(new Edge("C", "D", 3));
        edges.add(new Edge("A", "D", 30));
        edges.add(new Edge("A", "C", 20));
        return edges;
    }
}
