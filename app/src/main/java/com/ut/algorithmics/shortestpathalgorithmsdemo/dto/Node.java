package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

public class Node<E> {
    private int index;
    private NodeInfo info;
    private LinkedList<NodeNeighbor> neighbors;

    public Node(){
        this.neighbors = new LinkedList<>();
    }

    public Node(int index, NodeInfo info){
        this.index = index;
        this.info = info;
        this.neighbors = new LinkedList<>();
    }

    public void addNeighbor(Node neighbor, double weight){
        NodeNeighbor nodeNeighbor = new NodeNeighbor(neighbor, weight);
        neighbors.add(nodeNeighbor);
    }

    public int getIndex(){
        return index;
    }

    public NodeInfo getInfo() {
        return info;
    }

    @Override
    public boolean equals(Object object){
        Node node = (Node)object;
        return node.getInfo().equals(this.getInfo());
    }

    public LinkedList<NodeNeighbor> getSortedNeighbors(){
        Collections.sort(neighbors, new Comparator<NodeNeighbor>() {
            @Override
            public int compare(NodeNeighbor o1, NodeNeighbor o2) {
                return o1.getNode().getInfo().compare(o2.getNode().getInfo());
            }
        });
        return neighbors;
    }

    public LinkedList<NodeNeighbor> getNeighbors() {
        return neighbors;
    }

    public int getNumberOfNeighbors(){
        return this.neighbors.size();
    }

    public Node getNeighborByIndex(int i){
        return this.neighbors.get(i).getNode();
    }
}
