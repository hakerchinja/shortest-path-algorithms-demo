package com.ut.algorithmics.shortestpathalgorithmsdemo.dto;

import com.ut.algorithmics.shortestpathalgorithmsdemo.helper.Helper;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Obstacle {
    private boolean isCircle;
    private int x;
    private int y;
    private int w;
    private int h;

    private double cost;

    public Obstacle(int x, int y, int w, int h, double cost, boolean isCircle){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.cost = cost;
        this.isCircle = isCircle;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public boolean isCircle() {
        return isCircle;
    }

    public void setCircle(boolean circle) {
        isCircle = circle;
    }

    public boolean isInObstacle(int px, int py){
        return isCircle ? Helper.isInCircle(px, py, x, y, w) : Helper.isInRect(px, py, x, y, w, h);
    }
}
