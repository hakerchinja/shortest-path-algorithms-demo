package com.ut.algorithmics.shortestpathalgorithmsdemo.service.impl;

import com.ut.algorithmics.shortestpathalgorithmsdemo.algorithms.AStarSearch;
import com.ut.algorithmics.shortestpathalgorithmsdemo.dto.*;
import com.ut.algorithmics.shortestpathalgorithmsdemo.service.AlgorithmsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlgorithmsServiceImpl implements AlgorithmsService {

    @Override
    public List<NodeInfo> startBfs(NodeInfo startPoint, List<NodeInfo> pointList, List<Edge> edgeList) {
        Graph graph = new Graph(pointList, edgeList, false);
        List<NodeInfo> nodeInfos = graph.bfs(graph.getNodeByLabel(startPoint.getLabel()).getIndex());
        return nodeInfos;
    }

    @Override
    public List<NodeInfo> startDfs(NodeInfo startPoint, List<NodeInfo> pointList, List<Edge> edgeList) {
        Graph graph = new Graph(pointList, edgeList, false);
        List<NodeInfo> nodeInfos = graph.dfs(startPoint.getLabel());
        return nodeInfos;
    }

    @Override
    public List<NodeInfo> startDijkstra(NodeInfo startPoint, NodeInfo endPoint, List<NodeInfo> pointList, List<Edge> edgeList) {
        Graph graph = new Graph(pointList, edgeList, false);
        int startPointIndex = graph.getNodeByLabel(startPoint.getLabel()).getIndex();
        int endPointIndex = graph.getNodeByLabel(endPoint.getLabel()).getIndex();
        List<NodeInfo> nodeInfos = graph.dijkstra(startPointIndex, endPointIndex);
        return nodeInfos;
    }

    @Override
    public List<NodeInfo> startAStar(NodeInfo startPoint, NodeInfo endPoint, List<Obstacle> obstacleList, int width, int height) {
        AStarSearch aStarSearch = new AStarSearch(width, height, obstacleList);
        List<AStarNode> nodes = aStarSearch.findShortestPath(startPoint.getX(), startPoint.getY(),
                endPoint.getX(), endPoint.getY());

        List<NodeInfo> nodeInfos = new ArrayList<>();
        for(AStarNode node : nodes) {
            nodeInfos.add(new NodeInfo(node.toString(), node.getX(), node.getY()));
        }
        return nodeInfos;
    }
}
