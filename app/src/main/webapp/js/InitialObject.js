var InitialObject = {
    "startPoint" : window.startPoint,
    "endPoint"   : window.endPoint,
    "pointList"  : window.pointList,
    "edgeList"   : window.edgeList,
    "obstacles"  : window.obstacleList,
    "width"      : 500,
    "height"     : 450
};

window.initialObject = InitialObject;

