var Algorithm = {
    "BFS": "bfs",
    "DFS": "dfs",
    "Dijkstra": "dijkstra",
    "AStar": "astar"
};

window.Algorithm = Algorithm;