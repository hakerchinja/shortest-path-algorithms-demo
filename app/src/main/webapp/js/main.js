/*
Main app
* */
var ID = {
    otherButtons: "#otherButtons",
    aStarButtons: "#aStarButtons",
    inputWeight: "#inputWeight"
};

$(document).ready(function () {
    //init
    var bfsObject = window.initialObject;
    var dfsObject = window.initialObject;
    var dijkstraObject = window.initialObject;
    var aStarObject = window.initialObject;

    var current = window.Algorithm.BFS;
    var currentObject = bfsObject;
    window.currentObject = currentObject;
    window.main.init(true, currentObject, current);
    var setWeight = false;

    var aStarButtonGroup = document.getElementById("aStarButtons");
    var otherButtonGroup = document.getElementById("otherButtons");
    var inputWeight = document.getElementById("inputWeight");

    aStarButtonGroup.style.display = "none";
    otherButtonGroup.style.display = "block";
    inputWeight.style.display = "none";

    //radio buttons
    $("#control_01").click(function () {
        aStarButtonGroup.style.display = "none";
        otherButtonGroup.style.display = "block";
        inputWeight.style.display = "none";
        current = window.Algorithm.BFS;
       currentObject = bfsObject;
       window.main.init(true, currentObject, current);
       window.draw.draw();
    });

    $("#control_02").click(function () {
        aStarButtonGroup.style.display = "none";
        otherButtonGroup.style.display = "block";
        inputWeight.style.display = "none";
        current = window.Algorithm.DFS;
       currentObject = dfsObject;
       window.main.init(true, currentObject, current);
       window.draw.draw();
    });

    $("#control_03").click(function () {
        aStarButtonGroup.style.display = "none";
        otherButtonGroup.style.display = "block";
        inputWeight.style.display = "block";
        current = window.Algorithm.Dijkstra;
        currentObject = dijkstraObject;
        window.main.init(true, currentObject, current);
        window.draw.draw();
    });

    $("#control_04").click(function () {
        aStarButtonGroup.style.display = "block";
        otherButtonGroup.style.display = "none";
        inputWeight.style.display = "block";
        current = window.Algorithm.AStar;
        currentObject = aStarObject;
        window.main.init(false, currentObject, current);
    });

    $("#btnStart").click(function () {
        if(current === window.Algorithm.BFS) {
            console.info(bfsObject.edgeList);
            ConnectApi.bfs(bfsObject, function(data){
               // console.log(data);
                window.allAlgorithmsComponent.startSimulation(data);
            });
        } else if (current === window.Algorithm.DFS) {
            ConnectApi.dfs(dfsObject, function(data){
                window.allAlgorithmsComponent.startSimulation(data);
            });
        } else if (current === window.Algorithm.Dijkstra) {
            ConnectApi.dijkstra(dijkstraObject, function(data){
                window.allAlgorithmsComponent.startSimulation(data);
            });
        } else if (current === window.Algorithm.AStar) {
            ConnectApi.astar(aStarObject, function(data){
                window.aStarComponent.startSimulation(data);
            });
        } else {
            console.error("Algorithm is not selected!");
        }
    });

    //control panel
    $("#btnNode").click(function () {
        if($("#inputNode").val() != "") {
            var node = {
                "label": $("#inputNode").val(),
                "x": Math.round(Math.random() * 500),
                "y": Math.round(Math.random() * 450)
            };

            if (node != null) {
                window.allAlgorithmsComponent.addNode(node);
            }
        }
    });

    $("#btnEdge").click(function () {
        if($("#inputNode1").val() != "" && $("#inputNode2").val() != "") {
            var edge = {
                "nodeFrom": $("#inputNode1").val(),
                "nodeTo": $("#inputNode2").val(),
                "weight": 10.0
            };

            if($("#inputWeight").val() != "") {
                edge.weight = Number($("#inputWeight").val());
            };

            if (edge != null) {
                window.allAlgorithmsComponent.addEdge(edge);

            };
        };
    });
    
    $("#btnObstacleRect").click(function () {
        if($("#inputWidth").val() != "" && $("#inputHeight").val() != "") {
            var rect = {
                "isCircle": false,
                "x": Math.round(Math.random() * 200) + 200,
                "y": Math.round(Math.random() * 150) + 150,
                "w": Number($("#inputWidth").val()),
                "h": Number($("#inputHeight").val()),
                "cost": Number($("#inputCostRect").val())
            };

            if(rect != null) {
                window.aStarComponent.addObstacle(rect);
            }

            $("#inputWidth").val("");
            $("#inputHeight").val("");
            $("#inputCostRect").val("");
        }
    });

});

function udpateStartPoint (newPoint) {
    window.currentObject.startPoint = newPoint;
}

window.udpateStartPoint = udpateStartPoint;