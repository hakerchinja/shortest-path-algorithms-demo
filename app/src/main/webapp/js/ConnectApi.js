(function () {
    function bfs(data, callback) {
        $.ajax({
            url:"/algorithms/bfs",
            type:"POST",
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function( data ) {
                console.log("bfs");
                if(callback){
                    callback(data);
                }
            }
        })
    };
    
    function dfs(data, callback) {
        $.ajax({
            url:"/algorithms/dfs",
            type:"POST",
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function( data ) {
            console.log("dfs");
            if(callback){
                callback(data);
            }
        }
    })
    };
    
    function dijkstra(data, callback) {
        $.ajax({
            url:"/algorithms/dijkstra",
            type:"POST",
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function( data ) {
                console.log("dijkstra");
                if(callback){
                    callback(data);
                }
            }
        })
    };

    function astar(data, callback) {
        $.ajax({
            url:"/algorithms/astar",
            type:"POST",
            data: JSON.stringify(data),
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function( data ) {
                console.log("astar");
                if(callback){
                    callback(data);
                }
            }
        })
    };

    ConnectApi = {bfs: bfs, dfs: dfs, dijkstra: dijkstra, astar: astar};
})();

