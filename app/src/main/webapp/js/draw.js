var ID = {
    container: "#container",
    addNode: "#addNode",
    addEdge: "#addEdge"
};

var COLORS = {
    startPoint: "#DF6C4F",
    endPoint: "#f7e847",
    strokeColor: "#36383a",
    rectObstacleColor: "#2cdf8f",
    circleObstacleColor: "#f7e847"
};

var main = (function(){
    var width = 500, height = 450;

    var init = function(isOthers, objectData, algorithm){
        if(isOthers){
            allAlgorithmsComponent.init(width, height, translateDifferentDataDueToDevelopers(objectData), algorithm);
        } else {
            aStarComponent.init(width, height, objectData);
        }
    };

    return {
        init: init
    }
})();

var allAlgorithmsComponent = (function(){

    var data;

    var init = function(width, height, objectData, algorithm){
        draw.init(width, height, algorithm);
        data = objectData;
        draw.initData(data);
        draw.draw();
    };

    var addNode = function(obj){
        draw.addNode(obj);
    };

    var addEdge = function(obj){
        draw.addEdge(obj);
    };

    var startSimulation = function(data){
       draw.startSimulation(data);
    };

    return {
        init: init,
        addNode: addNode,
        addEdge: addEdge,
        startSimulation: startSimulation
    };

})();

var aStarComponent = (function(){

    var data;

    var init = function(width, height, objectData){
        drawAStar.init(width, height);
        data = objectData;
        drawAStar.initData(data);
        drawAStar.draw();
    };

    var addObstacle = function(obstacle){
        drawAStar.addObstacle(obstacle);
    };

    var startSimulation = function(data){
        drawAStar.startSimulation(data);
    };

    return {
        init: init,
        addObstacle: addObstacle,
        startSimulation: startSimulation
    };

})();

var draw = (function(){
    var svg, width, height;
    var nodes, edges;
    var startPoint, endPoint;

    var lines, lineAttributes;

    var last;

    var algorithm;

    var currentNode, previousNode, simulationData, counter;

    var init = function(w, h, alg){
        svg = d3.select(ID.container);

        width = w;
        height = h;

        algorithm = alg;

        currentNode = null, previousNode = null;
    };

    var initData = function(data){
        nodes = data.nodes;
        edges = data.edges;
        startPoint = data.startPoint;
        endPoint = data.endPoint;
    };

    var addNode = function(obj){
        nodes.push(obj);
        draw();
    };

    var addEdge = function(obj){
        edges.push(obj);
        draw();
    };

    var draw = function(){
        svg.selectAll("line").remove();
        svg.selectAll("g").remove();

        lines = svg.selectAll("line")
                        .data(edges)
                        .enter()
                        .append("line");
        //console.log(lines);
        lines.exit().remove();

        lineAttributes = lines
            .attr("x1", (d)=> getXForLabel(d.nodeFrom))
            .attr("y1", (d)=> getYForLabel(d.nodeFrom))
            .attr("x2", (d)=> getXForLabel(d.nodeTo))
            .attr("y2", (d)=> getYForLabel(d.nodeTo))
            .attr("stroke-width", (d)=>d.weight)
            .attr("stroke", COLORS.strokeColor)
            .attr("opacity", 1)
            .attr("id", (d)=> d.nodeFrom + "-" + d.nodeTo);

        var svgG = svg.selectAll("g")
            .data(nodes);

        svgG.exit().remove();

        var elemEnter = svgG.enter()
            .append("g")
            .attr("transform", (d) => `translate(${d.x}, ${d.y})`)
            .attr("label", (d) => d.label)
            .call(d3.drag()
                .on("start", dragStarted)
                .on("drag", dragged)
                .on("end", dragEnded))
            ;

        var circles = elemEnter.append("circle")
            .attr("r", 20)
            .attr("stroke", COLORS.strokeColor)
            .attr("fill", (d) => {
                return d.label === startPoint ? COLORS.startPoint : (d.label === endPoint && algorithm==window.Algorithm.Dijkstra) ? COLORS.endPoint : "white";
            })
            .attr("stroke", (d) => {
                return d.label === startPoint ? COLORS.startPoint : (d.label === endPoint && algorithm==window.Algorithm.Dijkstra) ? COLORS.endPoint : "white";
            })
            .attr("id", (d) => d.label);

        elemEnter.append("text")
            .attr("dx", -5)
            .attr("dy", 5)
            .text((d) => d.label);
    };

    var getXForLabel = function(label){
        for(var i=0;i<nodes.length;i++){
            if(nodes[i].label === label){
                return nodes[i].x;
            }
        }
        return -1;
    };

    var getYForLabel = function(label){
        for(var i=0;i<nodes.length;i++){
            if(nodes[i].label === label){
                return nodes[i].y;
            }
        }
        return -1;
    };

    var dragStarted = function(d){
        d3.select(this).raise().classed("active", true);
        lineAttributes.attr("opacity", 0);
    };

    var dragged = function(d){
        //"cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y
        d3.select(this).attr("transform", `translate(${d3.event.x}, ${d3.event.y})`);
    };

    var dragEnded = function(d){
        d3.select(this).classed("active", false);

        var label = d3.select(this).attr("label");
        var string = d3.select(this).attr("transform");
        var translate = string.substring(string.indexOf("(")+1, string.indexOf(")")).split(",");

        var x = parseInt(translate[0]);
        var y = parseInt(translate[1]);

        if(checkIfIsSame(label, x, y)){
            //click event
            var isDoubleClick = false;
            var newTime = + new Date();
            if(last){
                if(newTime - last < 200){
                    isDoubleClick = true;
                }
             }

            last = newTime;

            if(isDoubleClick){
                startPoint = label;

                point = {
                    "label": label,
                    "x": getXForLabel(label),
                    "y": getYForLabel(label)
                };

                window.udpateStartPoint(point);
            }
        }
        else {
            if(x < 0 || x > width || y < 0 || y > height){
                deleteNode(label);
            } else {
                changeNodeCoordinates(label, x, y);
            }
        }

        draw();

       lineAttributes.attr("opacity", 1);
    };

    var changeNodeCoordinates = function(label, x, y){
        for(var i=0;i<nodes.length;i++){
            var node = nodes[i];
            if(node.label === label){
                nodes[i].x = x;
                nodes[i].y = y;
                break;
            }
        }
    };

    var deleteNode = function(label){
        for(var i=nodes.length-1;i>=0;i--){
            if(nodes[i].label === label){
                nodes.splice(i, 1);
                break;
            }
        }

        for(var i=edges.length-1;i>=0;i--){
            if(edges[i].nodeFrom === label || edges[i].nodeTo === label){
                edges.splice(i, 1);
            }
        }

        if(startPoint === label && nodes.length > 0){
            startPoint = nodes[0].label;
        }

        if(endPoint === label && nodes.length > 0){
            endPoint = nodes[nodes.length  - 1].label;
        }
    };

    var checkIfIsSame = function(label, x, y){
        for(var i=0;i<nodes.length;i++){
            var node = nodes[i];
            if(node.label === label){
                return  node.x === x && node.y === y;
            }
        }
        return false;
    };

    var startSimulation = function(data){
        //console.log("tuka sum");
        simulationData = data;
        counter = 0;
        currentNode = simulationData[counter];
        previousNode = null;
        simulation();
    };

    var simulation = function(){
        setTimeout(function(){
            d3.select("#" + currentNode.label).style("fill", "salmon");

            if(previousNode != null){
                var selector = getEdge();
                //var selector = `#${previousNode.label}-${currentNode.label}`;
                d3.select(selector).style("stroke", "salmon");
            }

            counter++;
            if(counter < simulationData.length){
                //console.log(currentNode);
                previousNode = currentNode;
                currentNode = simulationData[counter];
                simulation();
            }
        }, 1000);
    };

    var getEdge = function(){
        for(var i=0;i<counter;i++){
            var from = simulationData[i].label;
            for(var j=0;j<edges.length;j++){
                if((edges[j].nodeFrom === from && edges[j].nodeTo === currentNode.label)){
                    //console.log(`#${edges[j].nodeFrom}-${edges[j].nodeTo}`);
                    return `#${edges[j].nodeFrom}-${edges[j].nodeTo}`;
                }
                if(edges[j].nodeTo === from && edges[j].nodeFrom === currentNode.label){
                    return `#${edges[j].nodeFrom}-${edges[j].nodeTo}`;
                }
            }
        }
        return "#a";
    };

    return {
        init: init,
        initData: initData,
        draw: draw,
        addNode: addNode,
        addEdge: addEdge,
        startSimulation: startSimulation
    };
})();

var drawAStar = (function(){
    var svg;
    var width, height;

    var startPoint, endPoint;
    var circles, rectangles, nodes;
    var data;

    var currentNode, previousNode;
    var simulationData, counter;

    var init = function(w, h){
        svg = d3.select(ID.container);
        width = w;
        height = h;
    };

    var initData = function(d){
        data = d;
        startPoint = d.startPoint;
        endPoint = d.endPoint;
        circles = [], rectangles = [], nodes = [];
        for(var i=0;i<d.obstacles.length;i++){
            if(d.obstacles[i].isCircle){
                circles.push(d.obstacles[i]);
            } else {
                rectangles.push(d.obstacles[i]);
            }
        }
        startPoint.label = "A";
        endPoint.label = "B";
        nodes.push(startPoint);
        nodes.push(endPoint);
    };

    var draw = function(draw){
        svg.selectAll("line").remove();
        svg.selectAll("g").remove();
        svg.selectAll("rect").remove();
        svg.selectAll("circle").remove();
        svg.selectAll(".circle-obstacles").remove();
        svg.selectAll(".rect-obstacles").remove();

        //draw start and end point
        var svgG = svg.selectAll("g")
            .data(nodes);

        svgG.exit().remove();

        var elemEnter = svgG.enter()
            .append("g")
            .attr("transform", (d) => `translate(${d.x}, ${d.y})`)
            .attr("label", (d) => d.label)
            .attr("id", (d)=> d.label)
            .call(d3.drag()
                .on("start", dragStarted)
                .on("drag", dragged)
                .on("end", dragEnded));

        var circlesElements = elemEnter.append("circle")
            .attr("r", 20)
            .attr("stroke", "white")
            .attr("fill", "white")
            .attr("id", (d)=>d.label);

        elemEnter.append("text")
            .attr("dx", -5)
            .attr("dy", 5)
            .text((d) => d.label);

        //end draw start and end point

        //draw circle obstacles
        var svgCircles = svg.append("g")
            .attr("class", "circle-obstacles");

        var svgGCircleObstacles = svgCircles.selectAll("g")
            .data(circles);

        svgGCircleObstacles.exit().remove();

        var elemEnter1 = svgGCircleObstacles.enter()
            .append("g")
            .attr("transform", (d) => `translate(${d.x}, ${d.y})`)
            .attr("label", (d,i) => i)
            .attr("id", (d, i) => i)
            .call(d3.drag()
                .on("start", dragStartedCircles)
                .on("drag", draggedCircles)
                .on("end", dragEndedCircles));

        var circles1 = elemEnter1.append("circle")
            .attr("r", (d)=>d.w)
            .attr("stroke", COLORS.circleObstacleColor)
            .attr("fill", COLORS.circleObstacleColor);

        elemEnter1.append("text")
            .attr("dx", -5)
            .attr("dy", 5)
            .text((d) => d.cost + "x");


        //draw rectange obstacles
        var svgRects = svg.append("g")
            .attr("class", "rect-obstacles");

        var svgGRectObstacles = svgRects.selectAll("g")
            .data(rectangles);

        svgGRectObstacles.exit().remove();

        var elemEnter2 = svgGRectObstacles.enter()
            .append("g")
            .attr("transform", (d) => `translate(${d.x}, ${d.y})`)
            .attr("label", (d,i) => i)
            .attr("id", (d, i) => i)
            .call(d3.drag()
                .on("start", dragStartedRects)
                .on("drag", draggedRects)
                .on("end", dragEndedRects));

        var rects = elemEnter2.append("rect")
            .attr("width", (d)=>d.w)
            .attr("height", (d)=>d.h)
            .attr("stroke", COLORS.rectObstacleColor)
            .attr("fill", COLORS.rectObstacleColor);

        elemEnter2.append("text")
            .attr("dx", (d)=>d.w/2-5)
            .attr("dy", (d)=>d.h/2+5)
            .attr("stroke", COLORS.strokeColor)
            .text((d) => d.cost + "x");
    };

    var dragStarted = function(d){
        d3.select(this).raise().classed("active", true);
    };

    var dragged = function(d){
        d3.select(this).attr("transform", `translate(${d3.event.x}, ${d3.event.y})`);
    };

    var dragEnded = function(d){
        d3.select(this).classed("active", false);

        var label = d3.select(this).attr("label");
        var string = d3.select(this).attr("transform");
        var translate = string.substring(string.indexOf("(")+1, string.indexOf(")")).split(",");

        var x = parseInt(translate[0]);
        var y = parseInt(translate[1]);

        if(x >= 0 && x < width && y >= 0 && y < height){
            changeNodeCoordinates(label, x, y);
        }

        draw();
    };

    var dragStartedCircles = function(d){
        d3.select(this).raise().classed("active", true);
    };

    var draggedCircles = function(d){
        d3.select(this).attr("transform", `translate(${d3.event.x}, ${d3.event.y})`);
    };

    var dragEndedCircles = function(d){
        d3.select(this).classed("active", false);

        var label = parseInt(d3.select(this).attr("label"));
        var string = d3.select(this).attr("transform");
        var translate = string.substring(string.indexOf("(")+1, string.indexOf(")")).split(",");

        var x = parseInt(translate[0]);
        var y = parseInt(translate[1]);

        if(x >= 0 && x < width && y >= 0 && y < height){
            changeCircleCoordinates(label, x, y);
        } else {
            deleteCircle(label);
        }

        draw();
    };

    var dragStartedRects = function(d){
        d3.select(this).raise().classed("active", true);
    };

    var draggedRects = function(d){
        d3.select(this).attr("transform", `translate(${d3.event.x}, ${d3.event.y})`);
    };

    var dragEndedRects = function(d){
        d3.select(this).classed("active", false);

        var label = parseInt(d3.select(this).attr("label"));
        var string = d3.select(this).attr("transform");
        var translate = string.substring(string.indexOf("(")+1, string.indexOf(")")).split(",");

        var x = parseInt(translate[0]);
        var y = parseInt(translate[1]);

        if(x >= 0 && x < width && y >= 0 && y < height){
            changeRectCoordinates(label, x, y);
        } else {
            deleteRect(label);
        }

        draw();
    };

    var changeRectCoordinates = function(i, x, y){
        rectangles[i].x = x;
        rectangles[i].y = y;
        window.changeRectangleCoordinates(i, x, y, window.currentObject);
    };

    var deleteRect = function(i){
        rectangles.splice(i, 1);
    };

    var changeCircleCoordinates = function(i, x, y){
        circles[i].x = x;
        circles[i].y = y;
    };

    var deleteCircle = function(i){
        circles.splice(i, 1);
    };

    var changeNodeCoordinates = function(label, x, y){
        if(label === "A"){
            startPoint.x = x;
            startPoint.y = y;
        }  else {
            endPoint.x = x;
            endPoint.y = y;
        }
        nodes = [];
        nodes.push(startPoint);
        nodes.push(endPoint);
    };

    var addObstacle = function(obstacle){
        window.addRectangleObstacle(obstacle, window.currentObject);
        obstacle.isCircle ? circles.push(obstacle) : rectangles.push(obstacle);
        draw();
    };

    var startSimulation = function(data){
        simulationData = data;
        //console.log(data);
        previousNode = null;
        currentNode = data[0];
        counter = 0;

        simulation();
    };

    var simulation = function(){
        setTimeout(function(){
            if(previousNode != null){
                svg.append("line")
                    .attr("id", counter)
                    .attr("x1", previousNode.x)
                    .attr("y1", previousNode.y)
                    .attr("x2", currentNode.x)
                    .attr("y2", currentNode.y)
                    .attr("stroke-width", 1)
                    .attr("stroke", "salmon")
                    .attr("opacity", 1);
            }

            counter++;
            if(counter < simulationData.length){
                previousNode = currentNode;
                currentNode = simulationData[counter];
                simulation();
            }
        }, 10);
    };

    return{
      init: init,
      initData: initData,
      draw: draw,
      addObstacle: addObstacle,
      startSimulation: startSimulation
    }
})();

var translateDifferentDataDueToDevelopers = (function (objectData) {
    return {
        nodes: objectData.pointList,
        edges: objectData.edgeList,
        startPoint: objectData.startPoint.label,
        endPoint: objectData.endPoint.label
    }
});

window.main.init = main.init;
window.allAlgorithmsComponent.addEdge = allAlgorithmsComponent.addEdge;
window.allAlgorithmsComponent.addNode = allAlgorithmsComponent.addNode;
window.draw.draw = draw.draw;