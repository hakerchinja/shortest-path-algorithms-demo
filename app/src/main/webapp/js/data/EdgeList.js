var EdgeList = [
    {
        "nodeFrom": "a",
        "nodeTo": "b",
        "weight": 2.0
    },
    {
        "nodeFrom": "a",
        "nodeTo": "c",
        "weight": 8.0
    },
    {
        "nodeFrom": "i",
        "nodeTo": "g",
        "weight": 10.0
    },
    {
        "nodeFrom": "d",
        "nodeTo": "f",
        "weight": 1.0
    },
    {
        "nodeFrom": "c",
        "nodeTo": "e",
        "weight": 8.0
    },
    {
        "nodeFrom": "h",
        "nodeTo": "i",
        "weight": 4.0
    },
    {
        "nodeFrom": "d",
        "nodeTo": "h",
        "weight": 3.0
    },
    {
        "nodeFrom": "a",
        "nodeTo": "j",
        "weight": 7.0
    },
    {
        "nodeFrom": "h",
        "nodeTo": "c",
        "weight": 5.0
    },
    {
        "nodeFrom": "j",
        "nodeTo": "h",
        "weight": 10.0
    },
    {
        "nodeFrom": "i",
        "nodeTo": "d",
        "weight": 11.0
    }
];

window.edgeList = EdgeList;
