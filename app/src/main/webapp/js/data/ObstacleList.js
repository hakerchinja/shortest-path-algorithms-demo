var ObstacleList = [
    {
        "isCircle": false,
        "x": 70,
        "y": 80,
        "w": 20,
        "h": 20,
        "cost": 2.0
    },
    {
        "isCircle": false,
        "x": 300,
        "y": 300,
        "w": 20,
        "h": 20,
        "cost": 4.0
    },
    {
        "isCircle": false,
        "x": 150,
        "y": 150,
        "w": 50,
        "h": 70,
        "cost": 4.0
    }
];

window.obstacleList = ObstacleList;