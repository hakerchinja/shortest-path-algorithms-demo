var PointList =  [
    {
        "label": "a",
        "x": 50,
        "y": 80
    },
    {
        "label": "b",
        "x": 220,
        "y": 60
    },
    {
        "label": "c",
        "x": 80,
        "y": 215
    },
    {
        "label": "d",
        "x": 210,
        "y": 425
    },
    {
        "label": "e",
        "x": 40,
        "y": 325
    },
    {
        "label": "f",
        "x": 70,
        "y": 405
    },
    {
        "label": "g",
        "x": 430,
        "y": 70
    },
    {
        "label": "h",
        "x": 235,
        "y": 300
    },
    {
        "label": "i",
        "x": 440,
        "y": 380
    },
    {
        "label": "j",
        "x": 310,
        "y": 180
    }
];

window.pointList = PointList;