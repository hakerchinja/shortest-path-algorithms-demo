var addNode = function (node, object) {
    var index = object.pointList.map(function (t) {
        return t['label'];
    }).indexOf(node.label);
    if(index > -1) {
        console.info("Node with label " + node.label + " already exists");
    } else {
        object.pointList.push(node);
        console.info("Node with label " + node.label + " is added successfully");
    }
};

window.addNode = addNode;