var addEdge = function (edge, object) {
    //setup
    indicesMatch = false;

    var indexFrom = object.pointList.map(function (t) {
        return t['label'];
    }).indexOf(edge.nodeFrom);
    var indexTo = object.pointList.map(function (t) {
        return t['label'];
    }).indexOf(edge.nodeTo);

    if(indexFrom > -1 && indexTo > -1) {
        for (var i = 0; i < object.edgeList.length; i++) {
            if (object.edgeList[i].nodeFrom === edge.nodeFrom) {
                if (object.edgeList[i].nodeTo === edge.nodeTo) {
                    indicesMatch = true;
                }
            }
        }

        if (indicesMatch) {
            console.info("Edge between node " + edge.nodeFrom + " and node " + edge.nodeTo + " already exists");
        } else {
            object.edgeList.push(edge);
            console.info("Edge between node " + edge.nodeFrom + " and node " + edge.nodeTo + " is added successfully");
        }
    } else {
        console.error("Node_From or Node_To are not added in the node list!");
    }
};

window.addEdge = addEdge;