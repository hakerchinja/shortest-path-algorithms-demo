# README #

# Interactive demo for path finding algorithms
The goal of this project is to provide a source for learning the most popular path finding algorithms in graphs. The interactive demo demo that is built has a simple UI and allows users to play around with the four popular algorithms: Breadth-First-Search (BFS), Depth-First-Search (DFS), Dijkstra's Algorithm and A*. The users can add and delete nodes and edges in the graph, select start and end nodes as well as add and delete obstacles. After the user creates the field, the algorithm is started and one can see how each of the algorithms work.

# Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

First you need a git tool.

	git https://hristijan_sardzoski@bitbucket.org/hakerchinja/shortest-path-algorithms-demo.git

Second, you need a tomcat server for running on local machine for development and testing services, possibly using :8080 or :8000 ports.
 
# Prerequisites
What things you need to install the software and how to install them.

For windows users, whether is 32bit or 64 bit machine.

    Eclipse IDE or IntelliJ IDEA(preffereable)
    Maven plugin
    Tomcat server embedded in Eclipse or Intellij
    Spring boot
    Java JDK 8.0 or higher
    
For installing Intellij IDEA use the following link. This comes with embedded tomcat server, maven plugin, and the ability to build Spring Boot application.
    
    https://www.jetbrains.com/idea/
 
For installing Eclipse IDE and using Tomcat, Maven, Spring Boot you need to download and manually set up all of them.
	
For manually downloading tomcat server:
https://www.eclipse.org/webtools/jst/components/ws/M5/tutorials/InstallTomcat.html

For installing Java JDK use the following link(9.0 preferable):
	
	http://www.oracle.com/technetwork/java/javase/downloads/index.html
    
# Running the application
After the setup of the required utilities, next is to explain how to start and use the application.

The application is a web application. The Web application is under /webapp. For using the web application Java JDK 8.0 or higher is required, alongside Tomcat Server. The application can be run as Spring Boot application. In this part, if using IntelliJ IDEA, one should import the /app web application and run the pom.xml file to get the required dependencies. After importing the project the setup of Application should take place. If for some reason, Intellij does not setup the application runner by itself(which will be very strange), Edit Configuration under Run section should be opened. There, first we add new configuration on the green plus sign under Spring Boot. Afterwards on the right side of the panel we should specify the name of the runner of the spring boot application, and add the main class which is the HealthyHeartApplication.java with the @SpringBootApplication annotation. Afterwards, the classpath of the module should be set to shortest-path-algorithms-demo and last but not the least the JRE path should be set to the destination of the JDK download folder as well. Make sure that in the pom.xml file you have the following dependency:

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

After this, the only thing left is to check wheter port :8080 is currently taken or not. If it is, make sure to free this port. At last, we can run the application and we should look it up under
	
    localhost:8080

If you receive the following using this url, than you have successfully set up the web application.

![Web Application](/app-image.png)

# Web application
The web application is divided into two parts:
		
    Java backend calculations
    Java script frontend visualization
 
 
The Web application is under /app. For using the web application Java JDK 8.0 or higher is required.

1. The Java backend calculations are performed using Spring Boot. The class seperation is the following:
	
    	-dto
            .Setup for graphs(nodes, edges in class representation)
        -algorithms
            .A*
            .BFS
            .DFS
            .Dijkstra
        -helper
            .Mathematical methods
   	 	-rest controllers
            .AlgorithmsController
    	-services and their implementations
            .AlgorithmsService
    	-the core spring boot application
    
    The dtos represent the objects that can be inferred from the input data. They have the attributes which correspond to the data needer for representation of a graph(undirected/directed).

    The rest controllers folder consist the AlgorithmsController where the java implemented algorithms are mapped for java script visualization using D3 visualization libary the urls:
	
   	 	/algorithms/bfs
     	/algorithms/dfs
     	/algorithms/dijkstra
     	/algorithms/astar
    
    The services folder consists the AlgorithmsSerivce, where the java algorithm implementations for BFS, DFS, Dijkstra and Astar are placed and exposed under the controller urls.

    The core spring boot application is used for running the application on server(in our case on local machine).
    
2. The Java script frontend visualization is the following:
 	
     	-js/connectApi
     	-js/javascript loader
    	-js/main javascript logic for visualization
        -js/draw
        -js/controlMethods (folder)
        -js/data (folder
    
    In the connectApi the connection between Java Algorithms controller and the main.js is established. Here we hit the four urls (/bfs, /dfs, /dijkstra and /astar) with ajax post callbacks.
    
    In the javascript loader the necessary javascripts are loaded. These are the following:
    	
        js/ConnectApi.js
        js/data/StartPoint.js
        js/data/EndPoint.js
        js/data/PointList.js
        js/data/EdgeList.js
        js/data/ObstacleList.js
        js/InitialObject.js
        js/Algorithm.js
        js/controlMethods/AddNode.js
        js/controlMethods/AddEdge.js
        js/controlMethods/AddCircleObstacle.js
        js/controlMethods/AddRectangleObstacle.js
        js/controlMethods/ChangeRectangleCoordinates.js
        js/main.js
        js/draw.js

        
     In the main.js the visualization of the four algorithms takes place. This part is separated in three vital subparts. 
     	
        -radio button selection of the algorithms
        -the svg where the simulation is drawn
        -the control panel for modifying the input graph
        
    
# Built With

Java - Spring Boot

JavaScript - D3.js library

Maven - Dependency Management

# Authors 
Simona Micevska 

Hristijan Sardjoski

# Aknowledments

Student Project

Advanced Algorithms 

Inspiration



 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
    